<?php
/**
 * Example of component
 *
 * @package BornDigital
 */

namespace BD\LoadPosts;

/**
 * Class to setup the component
 */
class Setup
{
	/**
	 * Current module directory
	 *
	 * @var string
	 */
	private $dir;

	/**
	 * Current module url
	 *
	 * @var string
	 */
	private $url;

	/**
	 * Setup the flow
	 */
	public function __construct()
	{
		$this->dir = MODULES_DIR . '/load-posts';
        $this->url = MODULES_URL . '/load-posts';
        
        //alll the hooks and filters

        add_action( 'wp_enqueue_scripts', [ $this, 'loadsetup' ] );
    }

   public function loadsetup()
   {
    wp_enqueue_script(
        'loadpost',
        $this->url . '/assets/js/load-posts.js',
        [],
        'auto',
        true
    );
   }

}

new Setup();
